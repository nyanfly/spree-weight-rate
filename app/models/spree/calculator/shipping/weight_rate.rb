module Spree
  module Calculator::Shipping
    class WeightRate < ShippingCalculator
      preference :fee_string, :text, default: "2:4,9:6.23"
      preference :handling_fee, :decimal, default: 0

      def self.description
        Spree.t(:weight_rate)
      end

      def available?(package)
        fee_hash = fee_string_to_hash(preferred_fee_string)

        fee_string_valid? && (package.weight < fee_hash.keys.max)
      end

      def compute_package(package)
        fee_hash = fee_string_to_hash(preferred_fee_string)

        weight_class = fee_hash.keys.select { |w| w >= package.weight }.min
        shipping_cost = fee_hash[weight_class]

        shipping_cost ? shipping_cost + preferred_handling_fee : 0
      end

      # Validates provided fee string
      def fee_string_valid?
        fee_string = preferred_fee_string.strip

        !fee_string.empty? &&
        fee_string.count(':') > 0 &&
        fee_string.split(/[:,]/)
      end

      def fee_string_to_hash(fee_string)
        Hash[*fee_string.split(/[:,]/).map(&:to_f)]
      end
    end
  end
end